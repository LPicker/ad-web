#!/usr/bin/env zx
import { $ } from 'zx';
import { rmSync } from 'fs';

await $`cat package.json | grep name`;

console.log('ad-web: Start executing the deployment process');
await $`git pull`;

console.log('ad-web: Start building');
await $`pnpm run build`;

rmSync('ad-web', { recursive: true, force: true });
await $`mv dist ad-web`;

console.log('ad-web: Start deploying');
await $`scp -i ~/.ssh/txy_cvm_all.pem -r ./ad-web ubuntu@119.45.131.60:/data/www/`;

rmSync('ad-web', { recursive: true, force: true });
console.log('Done!');
