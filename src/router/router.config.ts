import type { RouteRecordRaw } from 'vue-router';
import type { IPrivilege } from '@/types';
import BasicLayout from '@/layouts/BasicLayout.vue';

const homeRoute: RouteRecordRaw = {
  path: '/',
  name: 'index',
  meta: { title: '' },
  component: BasicLayout,
  children: [] as RouteRecordRaw[],
};

const welcomeRoute = {
  path: '/welcome',
  name: 'welcome',
  meta: { title: '首页' },
  component: () => import('../views-medium/WelcomePage.vue'),
};

// 前端路由映射表
const pageRouteMap: RouteRecordRaw[] = [
  // 菜单路由
  {
    path: '/settings',
    name: 'settings',
    meta: { title: '账号管理', flat: true },
    component: BasicLayout,
    redirect: () => ({ name: 'accountManagement' }),
    children: [],
  },
  {
    path: '/ad-management',
    name: 'adManagement',
    meta: { title: '广告管理', flat: true },
    component: BasicLayout,
    redirect: () => ({ name: 'appManagement' }),
    children: [],
  },
  {
    path: '/traffic-monitoring-allocation',
    name: 'trafficMonitoringAllocation',
    meta: { title: '流量监控分配', flat: false },
    component: BasicLayout,
    children: [],
  },
  {
    path: '/review-ad',
    name: 'reviewAd',
    meta: { title: '广告审核', flat: true },
    component: BasicLayout,
    redirect: () => ({ name: 'handleAudit' }),
    children: [],
  },

  // 页面路由
  {
    path: '/medium-management',
    name: 'mediumManagement',
    meta: { title: '媒体方管理' },
    component: () => import('../views-admin/settings/MediumManagement.vue'),
  },
  {
    path: '/account-management',
    name: 'accountManagement',
    meta: { title: '账号权限管理' },
    component: () => import('../views-admin/settings/AccountManagement.vue'),
  },

  {
    path: '/app-management',
    name: 'appManagement',
    meta: { title: '应用管理' },
    component: () => import('../views-admin/app/AppManagement.vue'),
  },
  {
    path: '/ad-space-management',
    name: 'adSpaceManagement',
    meta: { title: '广告位管理' },
    component: () => import('../views-admin/adSpace/AdSpaceManagement.vue'),
  },
  {
    path: '/ad-data-management',
    name: 'adDataManagement',
    meta: { title: '广告位数据' },
    component: () => import('../views-admin/adData/AdDataManagement.vue'),
  },

  {
    path: '/bidding-data',
    name: 'biddingData',
    meta: { title: '竞价数据' },
    component: () => import('../views-admin/trafficMonitoringAllocation/biddingData.vue'),
  },

  {
    path: '/handle-audit',
    name: 'handleAudit',
    meta: { title: '审核处理' },
    component: () => import('../views-admin/reviewAd/handleAudit.vue'),
  },
  {
    path: '/risk-control-handling',
    name: 'riskControlHandling',
    meta: { title: '风控处理' },
    component: () => import('../views-admin/reviewAd/riskControlHandling.vue'),
  },
  {
    path: '/handle-emergency',
    name: 'handleEmergency',
    meta: { title: '应急处理' },
    component: () => import('../views-admin/reviewAd/handleEmergency.vue'),
  },
];

/**
 * 格式化 后端 结构信息并递归生成层级路由表
 */
export const generator = (routerMap: IPrivilege, role: string) => {
  const menus = [];
  const routes: RouteRecordRaw[] = [];
  const isMedium = role === 'media';
  const _pageRouteMap = pageRouteMap.filter(item => (isMedium ? item.meta?.title !== '账号权限管理' : true));
  // if (isMedium) {
  menus.push(welcomeRoute);
  homeRoute.children.push(welcomeRoute);
  // }
  routes.push(homeRoute);

  Object.keys(routerMap).forEach(title => {
    const parent = _pageRouteMap.find(item => item.meta?.title === title);
    if (parent) {
      const parentMenu: RouteRecordRaw = {
        ...parent,
        children: routerMap[title]
          .map(subTitle => _pageRouteMap.find(item => item.meta?.title === subTitle))
          .filter(r => r) as RouteRecordRaw[],
      };
      menus.push(parentMenu);
      routes.push(parentMenu);
    }

    homeRoute.redirect = () => {
      const name = homeRoute.children[0]?.name || routes[1]?.name;
      return { name };
    };
  });

  return { menus, routes };
};
