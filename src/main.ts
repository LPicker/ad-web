import '@ant-design-vue/pro-layout/dist/style.css';

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { ConfigProvider } from 'ant-design-vue';
import ProLayout, { PageContainer } from '@ant-design-vue/pro-layout';
import router from './router';
import App from './App.vue';

import './permission'; // permission control
import action from './directives/action';

const app = createApp(App);
app.use(action);
app.use(createPinia());
app.use(router).use(ConfigProvider).use(ProLayout).use(PageContainer).mount('#app');
