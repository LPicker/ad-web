import request from '@/utils/request';

const api = {
  CreateUser: '/v1/user/add',
  ModifyUser: '/v1/user/update',
  DeleteUser: '/v1/user/delete',
  EnableUser: '/v1/user/status',
};

export function createUser(parameter: Record<string, unknown>) {
  return request({
    url: api.CreateUser,
    method: 'post',
    data: parameter,
  });
}

export function modifyUser(parameter: Record<string, unknown>) {
  return request({
    url: api.ModifyUser,
    method: 'put',
    data: parameter,
  });
}

export function deleteUser(id: string) {
  return request({
    url: `${api.DeleteUser}/${id}`,
    method: 'delete',
  });
}

export function enableUser(id: string) {
  return request({
    url: `${api.EnableUser}/${id}`,
    method: 'patch',
  });
}
