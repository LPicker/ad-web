/*
 * @Author: lianjianjie
 * @Date: 2024-03-30 18:44:11
 * @LastEditors: lianjianjie@foxmail.com
 * @LastEditTime: 2024-03-30 19:55:44
 * @Description: 广告审核相关接口
 */
import request from '@/utils/request';

export function addToEmergency(id: string, remark: string) {
  return request({
    url: '/v1/adreview/emergency',
    method: 'post',
    data: { id, remark },
  });
}

export function enabledAd(ids: string | string[], enabled: boolean, remark: string) {
  if (Array.isArray(ids)) {
    return request({
      url: '/v1/adreview/enabled/list',
      method: 'post',
      data: { ids, enabled, remark },
    });
  }
  return request({
    url: '/v1/adreview/enabled',
    method: 'post',
    data: { id: ids, enabled, remark },
  });
}
