import request from '@/utils/request';

interface ThousandShow {
  date: string;
  revenue: number;
}
export interface IMediaIndex {
  yesterday_income?: number;
  month_income?: number;
  year_income?: number;
  request?: number;
  return?: number;
  show?: number;
  click?: number;
  fill_rate?: number;
  ring_growth_fill_rate?: number;
  show_rate?: number;
  ring_growth_show_rate?: number;
  click_rate?: number;
  ring_growth_click_rate?: number;
  thounds_show?: ThousandShow[];
}

export function getMediaIndex(media_id?: string) {
  return request<IMediaIndex>({
    url: '/v1/data/mediaindex',
    method: 'get',
    params: { media_id },
  });
}

export function getMediaHistory(start: number, end: number, media_id?: string) {
  return request({
    url: '/v1/data/mediahistory',
    method: 'get',
    params: { start, end, media_id },
  });
}

export function exportAdData(ids: string[]) {
  return request({
    url: '/v1/data/export',
    method: 'post',
    responseType: 'blob',
    data: { ids },
  });
}
