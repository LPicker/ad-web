import request from '@/utils/request';

const userApi = {
  Login: '/v1/login',
  Logout: '/v1/user/logout',
  SendCaptcha: '/v1/authCode',
  GetPrivilegeList: '/v1/privilege',

  Register: '/auth/register',
  twoStepCode: '/auth/2step-code',
  SendSmsErr: '/account/sms_err',
  // get my info
  UserInfo: '/user/info',
  UserMenu: '/user/nav',
};

export function login(parameter: Record<string, unknown>) {
  return request({
    url: userApi.Login,
    method: 'post',
    data: parameter,
  });
}

export function getEmailCaptcha(email: string) {
  return request({
    url: userApi.SendCaptcha,
    method: 'post',
    data: { email },
  });
}

export function getPrivilegeList() {
  return request({
    url: userApi.GetPrivilegeList,
    method: 'get',
  });
}

export function getInfo() {
  return request({
    url: userApi.UserInfo,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });
}

export function logout() {
  return request({
    url: userApi.Logout,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });
}

/**
 * get user 2step code open?
 * @param parameter {*}
 */
export function get2step(parameter: Record<string, unknown>) {
  return request({
    url: userApi.twoStepCode,
    method: 'post',
    data: parameter,
  });
}
