import request from '@/utils/request';

const api = {
  CreateAdSite: '/v1/adsite/add',
  ModifyAdSite: '/v1/adsite/update',
  DeleteAdSite: '/v1/adsite/delete',
  EnableAdSite: '/v1/adsite/status',
  BiddingAdSite: '/v1/adsite/bidding',
  BatchDeleteAdSite: '/v1/adsite/batchdel',
};

export function createAdsite(parameter: Record<string, unknown>) {
  return request({
    url: api.CreateAdSite,
    method: 'post',
    data: parameter,
  });
}

export function modifyAdSite(parameter: Record<string, unknown>) {
  return request({
    url: api.ModifyAdSite,
    method: 'put',
    data: parameter,
  });
}

export function enableAdSite(id: string) {
  return request({
    url: `${api.EnableAdSite}/${id}`,
    method: 'patch',
  });
}

export function biddingAdSite(id: string) {
  return request({
    url: `${api.BiddingAdSite}/${id}`,
    method: 'patch',
  });
}


export function deleteAdSite(ids: string | string[]) {
  if (Array.isArray(ids)) {
    return request({
      url: api.BatchDeleteAdSite,
      method: 'post',
      data: { ids },
    });
  } else {
    return request({
      url: `${api.DeleteAdSite}/${ids}`,
      method: 'delete',
    });
  }
}
