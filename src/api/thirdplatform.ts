/*
 * @Author: lianjianjie
 * @Date: 2024-03-23 18:11:34
 * @LastEditors: lianjianjie@foxmail.com
 * @LastEditTime: 2024-03-23 20:10:22
 * @Description: 媒体相关接口
 */
import request from '@/utils/request';

const api = {
  CreateThirdadplatform: '/v1/thirdadplatform/add',
  ModifyThirdadplatform: '/v1/thirdadplatform/update',
  DeleteThirdadplatform: '/v1/thirdadplatform/delete',
};

export function createThirdadplatform(parameter: Record<string, unknown>) {
  return request({
    url: api.CreateThirdadplatform,
    method: 'post',
    data: parameter,
  });
}

export function modifyThirdadplatform(parameter: Record<string, unknown>) {
  return request({
    url: api.ModifyThirdadplatform,
    method: 'put',
    data: parameter,
  });
}

export function deleteThirdadplatform(id: string) {
  return request({
    url: `${api.DeleteThirdadplatform}/${id}`,
    method: 'delete',
  });
}
