/*
 * @Author: lianjianjie
 * @Date: 2024-03-23 18:11:34
 * @LastEditors: lianjianjie@foxmail.com
 * @LastEditTime: 2024-04-09 21:32:25
 * @Description: 媒体相关接口
 */
import request from '@/utils/request';

interface IMediumItem {
  id: string;
  name: string;
  secretKey: string;
  status: boolean;
}

interface IMediumData {
  list: IMediumItem[];
  count: number;
}

const api = {
  GetMedias: '/v1/media/list',
  CreateMedia: '/v1/media/add',
  ModifyMedia: '/v1/media/update',
  DeleteMedia: '/v1/media/delete',
  EnableMedia: '/v1/media/status',
};

export function getMedias(parameter: Record<string, unknown>) {
  return request<IMediumData>({
    url: api.GetMedias,
    method: 'get',
    params: parameter,
  });
}

export function createMedia(parameter: Record<string, unknown>) {
  return request({
    url: api.CreateMedia,
    method: 'post',
    data: parameter,
  });
}

export function modifyMedia(parameter: Record<string, unknown>) {
  return request({
    url: api.ModifyMedia,
    method: 'put',
    data: parameter,
  });
}

export function deleteMedia(id: string) {
  return request({
    url: `${api.DeleteMedia}/${id}`,
    method: 'delete',
  });
}

export function enableMedia(id: string) {
  return request({
    url: `${api.EnableMedia}/${id}`,
    method: 'patch',
  });
}
