import request from '@/utils/request';

const api = {
  CreateApp: '/v1/app/add',
  ModifyApp: '/v1/app/update',
  DeleteApp: '/v1/app/delete',
  BatchDeleteApp: '/v1/app/batchdel',
  EnableApp: '/v1/app/status',
};

export function createapp(parameter: Record<string, unknown>) {
  return request({
    url: api.CreateApp,
    method: 'post',
    data: parameter,
  });
}

export function modifyApp(parameter: Record<string, unknown>) {
  return request({
    url: api.ModifyApp,
    method: 'put',
    data: parameter,
  });
}

/**
 * 使用 id 删除应用
 *
 * @param ids - 应用的 id
 * 如果传了一个数据，会被认为是一个 id 组成的 list，然后做批量删除处理
 */
export function deleteApp(ids: string | string[]) {
  if (Array.isArray(ids)) {
    return request({
      url: api.BatchDeleteApp,
      method: 'post',
      data: { ids },
    });
  } else {
    return request({
      url: `${api.DeleteApp}/${ids}`,
      method: 'delete',
    });
  }
}

export function enableApp(id: string) {
  return request({
    url: `${api.EnableApp}/${id}`,
    method: 'patch',
  });
}
