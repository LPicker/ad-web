/*
 * @Author: lianjianjie
 * @Date: 2024-03-30 18:43:22
 * @LastEditors: lianjianjie@foxmail.com
 * @LastEditTime: 2024-03-30 18:56:53
 * @Description: 风控审核相关接口
 */
import request from '@/utils/request';

export function enabledAd(ids: string | string[], enabled: boolean, remark: string) {
  if (Array.isArray(ids)) {
    return request({
      url: '/v1/adrisk/enabled/list',
      method: 'post',
      data: { ids, enabled, remark },
    });
  }
  return request({
    url: '/v1/adrisk/enabled',
    method: 'post',
    data: { id: ids, enabled, remark },
  });
}
