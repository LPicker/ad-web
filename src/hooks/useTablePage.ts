import { ref } from 'vue';
import { useUserStore } from '@/store';

export enum PAGE_TYPE {
  VIEW = 'view',
  CREATING = 'creating',
  EDITING = 'editing',
}

const PERMISSION_ENUM = {
  create: { key: 'create', label: '新增' },
  delete: { key: 'delete', label: '删除' },
  edit: { key: 'edit', label: '修改' },
  emergency: { key: 'emergency', label: '查询' },
  enable: { key: 'enable', label: '启用' },
  disable: { key: 'disable', label: '禁用' },
};

export default () => {
  const pageType = ref<PAGE_TYPE | null>(null);

  /**
   * <a-button v-if="$auth('edit')">Button</a-button>
   */
  function auth(authKey: string) {
    const userStore = useUserStore();
    const isMedium = userStore.role === 'media';
    return isMedium ? false : authKey in PERMISSION_ENUM;
  }

  return { pageType, $auth: auth };
};
