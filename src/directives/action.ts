import type { Directive } from 'vue';
import { useUserStore } from '@/store';

/**
 * Action 权限指令
 * 指令用法：
 *  - 在需要控制 action 级别权限的组件上使用 v-action:[method] , 如下：
 *    <i-button v-action:create>添加用户</a-button>
 *    <a-button v-action:delete>删除用户</a-button>
 *    <a v-action:delete @click="edit(record)">修改</a>
 *    <a v-action:emergency @click="edit(record)">添加应急</a>
 *
 *  - 当前用户没有权限时，组件上使用了该指令则会被隐藏
 *  - 当后台权限跟 pro 提供的模式不同时，只需要针对这里的权限过滤进行修改即可
 *
 *  @see https://github.com/vueComponent/ant-design-vue-pro/pull/53
 */
const action: Directive<HTMLElement, null> = {
  created(el, binding) {
    const userStore = useUserStore();
    const isMedium = userStore.role === 'media';
    const actionName = binding.arg;
    if (isMedium && actionName && ['create', 'edit', 'delete', 'emergency'].includes(actionName)) {
      el.style.display = 'none';
    }
  },
};

export default {
  install(app: ReturnType<typeof createApp>) {
    app.directive('action', action);
  },
};
