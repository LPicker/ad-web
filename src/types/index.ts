import type { ColumnType } from 'ant-design-vue/es/table';

export interface IPrivilege {
  [title: string]: string[];
}

export type ILineSeriesData = number[][] | { name: string; data: number[] }[];

export interface IFunnelSeriesData {
  value: number;
  name: string;
}

export interface IRow {
  [key: string]: any;
  id: string;
}

export interface IOption {
  label: string;
  value: string;
}

interface IBaseColumn {
  title: string;
  dataIndex: string;
  align?: 'left' | 'center' | 'right';
  fixed?: 'left' | 'right';
  width?: string | number;
  required?: boolean;
  readonly?: boolean;
  searchable?: boolean;
  defaultValue?: string | number | boolean;
  customRender?: ColumnType<IRow>['customRender'];
}

interface ITextColumn extends IBaseColumn {
  type?: 'text';
}

interface IDateColumn extends IBaseColumn {
  type: 'date';
}

interface IDateTimeColumn extends IBaseColumn {
  type: 'dateTime';
}

interface IDateRangeColumn extends IBaseColumn {
  type: 'dateRange';
}

export interface IBooleanColumn extends IBaseColumn {
  type: 'boolean';
  switchLabels: [string, string];
}

export interface IEnumColumn extends IBaseColumn {
  type: 'enum';
  /** 枚举类型的列，必须指定枚举项 */
  options: IOption[];
}

export type IColumn = ITextColumn | IDateColumn | IDateTimeColumn | IDateRangeColumn | IBooleanColumn | IEnumColumn;

export interface ITableResponse {
  [key: string]: unknown;
  list: IRow[];
  count: number;
}

/**
 * 审核所需参数
 */
export interface IAuditParams {
  /** 批量处理时，为数组 */
  ids: string | string[];
  enabled: boolean;
  remark: string;
  data?: IRow;
}
