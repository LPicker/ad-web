import { defineStore } from 'pinia';
import useUserStore from './user';

// 全局通用状态
const useGlobalStore = defineStore('global', {
  state: () => {
    return {
      loading: false,
    };
  },
  actions: {},
});

export { useGlobalStore, useUserStore };
