#!/bin/bash

# 拉取最新的代码
git pull

# 添加所有变更到暂存区
git add .

# 提交变更
# 需要提供提交信息作为第一个参数
if [ -z "$1" ]; then
  echo "错误：未提供提交信息。"
  exit 1
fi
git commit -m "$1"

# 推送到远程仓库
git push
