# 阶段一：构建
# 假设你的前端项目可以通过npm run build来构建
FROM node:latest as builder
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build

FROM caddy
COPY --from=builder /app/dist /usr/share/caddy/pgad
COPY --from=builder /app/Caddyfile /etc/caddy/Caddyfile
EXPOSE 80 443
CMD ["caddy", "run", "--config", "/etc/caddy/Caddyfile", "--adapter", "caddyfile"]
