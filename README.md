# 城艺通广告管理系统

使用 Vue 3 + Ant Design Vue 3 + vite2 搭建的工程

### 安装依赖

使用 `pnpm` 管理依赖

```sh
pnpm i
```

### 本地运行开发服务器

```sh
pnpm dev
```

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's `.vue` type support plugin by running `Volar: Switch TS Plugin on/off` from VSCode command palette.

### 构建部署

一键部署：

- 自动拉取当前分支最新代码；
- 自动构建；
- 自动部署到服务器。

```sh
pnpm run deploy
```
